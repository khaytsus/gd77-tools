# gd77

Small repository of tools and data for the Radioddity GD-77

# codeplugs

Under the codeplugs directory, there are some sample codeplugs.  They are made for my use, but I an putting them here in case they are useful to anyone else.  They should be named basically of a format of area-cpsversion-date.dat

I am using the GD-77 Community Edition editor for all of my codeplug work as it gives me some more flexability.  The codeplugs in the above directory likely will not be compatable with the standard Radioddity CPS.

It can be gotten from https://github.com/rogerclarkmelbourne/radioddity_gd-77_cps/tree/master/installer 

# bin

Scripts, generally useful in Linux but could work in other similar environments, for doing useful things.

## csv2id.sh

This script pulls data from ham-digital.org for a number of defined areas to get the Last Heard information and builds a DMR ID csv file out of it which is ready to import into the Radioddity CPS "ActiveClient" DMR ID tool to upload to the radio.

And just to remind myself to read or wtite DMR ID data press SK2+Menu+# key at the same time and turn on radio.