#!/bin/sh

# Update these three variables as needed
# Change this to where you want your data to be kept
datapath="/home/wally/ham/Radios/GD77/SW/DMR"

# Array of id,name which the script iterates over to get data, update for your needs
# Here I am getting all US plus some targeted states I'm interested in which pulls
# in a lot more calls per those states.  I do not pull in 310 here; I do that
# separately as it's a much larger dataset.

ids=(311,us311 312,us312 313,us313 314,us314 315,us315 316,us316 \
     317,us317 318,us318 319,us319 320,us320 3121,ky 3117,il 3118,in \
     3139,oh 3147,tn)

# Radio holds 48000 with 16 chars + use voice prompt memory
max=48000

# Max per area
count=2000

# 310 area users..  3000 is about 200 days or so
uscount=15000

# How many extra for WW should we pull, there are always duplicates which
# winds up with not having nearly as many contacts as we can have, so pad
# the number a bit to get more contacts in once we de-duplicate everything
wwextra=5000

# Shouldn't need to touch below here

ts=`date +"%Y-%m-%d"`
output=${ts}.csv

cd ${datapath}

rm *-???.csv *-????.csv

# Download each csv file
for i in ${ids[@]}; do
    id=`echo $i | cut -f 1 -d ','`
    name=`echo $i | cut -f 2 -d ','`
    echo "Getting ${name} (id $id}) (${count} max)"
    curl -s -o ${name}-${id}.csv "https://ham-digital.org/user_by_lh.php?id=${id}&cnt=${count}"
done

# Pull last heard from the 310 area, limited to uscount
echo "Getting 310 Last Heard data (${uscount} max)"
curl -s -o us310-310.csv "https://ham-digital.org/user_by_lh.php?id=310&cnt=${uscount}"

# Combine all of them into a single file to count how many we currently have
awk 'BEGIN { FS=";"; OFS=","; } {print $3,$2 " " $4,$4,",,,,<br/>"}' *-???.csv *-????.csv | sort | uniq | grep -v 'dmrid,callsign' >${output}

# Check to see if we exceeded the maximum this radio allows
lines=`wc -l ${output} | cut -f 1 -d " "`
if [ ${lines} -gt ${max} ]; then
    echo "Warning 1:  CSV contains more than ${max} lines"
fi

# Whatever is left, pull it from world-wide
wwcount=$(($max-$lines+$wwextra))

echo ""
echo "Have ${lines} of data, getting ${wwcount} more from world-wide"
curl -s -o lh-0000.csv "https://ham-digital.org/user_by_lh.php?cnt=${wwcount}"

# Combine all of them into a final file formatted for import
awk 'BEGIN { FS=";"; OFS=","; } {print $3,$2 " " $4,$4,",,,,<br/>"}' *-???.csv *-????.csv | sort | uniq | grep -v 'dmrid,callsign' >${output}

# Check to see if we exceeded the maximum this radio allows
lines=`wc -l ${output} | cut -f 1 -d " "`
if [ ${lines} -gt ${max} ]; then
    echo "Warning 2:  CSV contains more than ${max} lines"
fi

missedcount=$(($max-$lines))

echo ""
echo "${datapath}/${output} with ${lines} (${missedcount} less than ${max}) contacts should be ready to import using ActiveClient.exe or OpenGD77 CPS"
echo "Set CPS to 16 chars, Use Voice Memory, and clear out Region"